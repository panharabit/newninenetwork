Rails.application.routes.draw do


  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :homes
  resources :blogs
  resources :ourteams
  resources :hostings
  resources :contacts
  resources :aboutus
  resources :whychooses
  resources :services
  root "homes#index"
  resources :categoryblogs do
    resources :blogs
  end
  resources :contactmes
end
