class CreateSliders < ActiveRecord::Migration[5.0]
  def change
    create_table :sliders do |t|
      t.string :saysomething
      t.string :title
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
