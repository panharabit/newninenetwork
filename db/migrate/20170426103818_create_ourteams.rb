class CreateOurteams < ActiveRecord::Migration[5.0]
  def change
    create_table :ourteams do |t|
      t.string :name
      t.string :position
      t.text :description

      t.timestamps
    end
  end
end
