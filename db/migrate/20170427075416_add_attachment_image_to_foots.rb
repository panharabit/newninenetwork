class AddAttachmentImageToFoots < ActiveRecord::Migration
  def self.up
    change_table :foots do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :foots, :image
  end
end
