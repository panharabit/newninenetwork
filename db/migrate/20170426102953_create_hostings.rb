class CreateHostings < ActiveRecord::Migration[5.0]
  def change
    create_table :hostings do |t|
      t.string :title
      t.string :image
      t.integer :space
      t.string :register
      t.string :support
      t.integer :price

      t.timestamps
    end
  end
end
