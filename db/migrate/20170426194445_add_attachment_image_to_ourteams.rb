class AddAttachmentImageToOurteams < ActiveRecord::Migration
  def self.up
    change_table :ourteams do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :ourteams, :image
  end
end
