class AddAttachmentImageToWhychooseus < ActiveRecord::Migration
  def self.up
    change_table :whychooseus do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :whychooseus, :image
  end
end
