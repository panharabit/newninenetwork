class AddAttachmentImageToHostings < ActiveRecord::Migration
  def self.up
    change_table :hostings do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :hostings, :image
  end
end
