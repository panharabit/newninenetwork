class AddAttachmentImageToAboutus < ActiveRecord::Migration
  def self.up
    change_table :aboutus do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :aboutus, :image
  end
end
