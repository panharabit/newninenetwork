class CreateBlogs < ActiveRecord::Migration[5.0]
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :description
      t.string :image
      t.date :post_date
      t.references :categoryblog, foreign_key: true, index: true

      t.timestamps
    end
  end
end
