class CreateFoots < ActiveRecord::Migration[5.0]
  def change
    create_table :foots do |t|
      t.string :image
      t.text :description
      t.string :copyright

      t.timestamps
    end
  end
end
