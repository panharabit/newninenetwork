class AddAttachmentImageToAddtionalsections < ActiveRecord::Migration
  def self.up
    change_table :addtionalsections do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :addtionalsections, :image
  end
end
