class ContactmesController < ApplicationController

  def index
    @footer = Foot.all.limit(1)
  end


    def new
      @contactme = Contactme.new
      @contact = Contact.all
      @footer = Foot.all.limit(1)
    end

    def create
          @contact = Contact.all
        @footer = Foot.all.limit(1)
      @contactme = Contactme.new(contactme_params)
      if @contactme.valid?
        NoticeMailer.sendmail_confirm(@contactme).deliver
        flash[:notice] = "Message Sent"
        redirect_to @contactme
      else
        render :action => 'new'
      end
    end

    private
      def contactme_params
        params.require(:contactme).permit(:firstname,:lastname,:email,:message,:phone)
      end
end
