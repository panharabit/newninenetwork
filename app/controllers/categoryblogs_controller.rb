
class CategoryblogsController < ApplicationController
  def index

    @footer = Foot.all.limit(1)
    @categoryblog = Categoryblog.find(params[:categoryblog_id])
    @blogs = @categoryblog.blogs
  end

  def show
    @footer = Foot.all.limit(1)
    @categoryblog = Categoryblog.find(params[:id])
    @blogs = @categoryblog.blogs
    @categoryblogs = Categoryblog.all
  end


    def new
      @categoryblog = Categoryblog.new
    end

    def create
      @categoryblog = Categoryblog.new(params_categoryblog)
      @categoryblog.save
    end

    private
      def params_categoryblog
        params.require(:categoryblog).permit(:title,:description,:post_date,:image,:categoryblog_id)
      end
end
