class HostingsController < ApplicationController
  def index
    @footer = Foot.all.limit(1)
    @hostplan = Addtionalsection.where({id: [5,6,7]}).limit(3)
    @pricing = Hosting.all.limit(4)
    @ourteam = Ourteam.all.limit(10)
  end
end
