class BlogsController < ApplicationController
  def index
    @footer = Foot.all.limit(1)
    @blog = Blog.all
    @categoryblog = Categoryblog.all
  end

  def show
    @footer = Foot.all.limit(1)
    @categoryblog = Categoryblog.find(params[:id])
    @blogs = @categoryblog.blogs
    @blog = Blog.find(params[:id])
  end

  def new
    @blog = Blog.new
  end

  def create
    @blog = Blog.new(params_blog)
    @blog.save
  end

  private
    def params_blog
      params.require(:blog).permit(:title,:description,:post_date,:image,:categoryblog_id)
    end
end
