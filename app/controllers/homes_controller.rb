class HomesController < ApplicationController
  def index
    @slides = Slider.limit(4)
    @whychooseus = Whychooseu.all.limit(3)
    @pricing = Hosting.all.limit(4)
    @addtionalsection = Addtionalsection.all.limit(1)
    @ourteam = Ourteam.all.limit(5)
    @hostingsection = Addtionalsection.where({id: [2,3,4]}).limit(3)
    @footer = Foot.all.limit(1)
  end

end
