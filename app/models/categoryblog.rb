class Categoryblog < ApplicationRecord
  has_many :blogs
  validates :categoryname,presence: true
end
