class Blog < ApplicationRecord
  has_attached_file :image, styles: {
  medium: "300x300>",
  thumb: "100x100>"
}
validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
validates :title,:description,:image,:post_date,presence: true
belongs_to :categoryblog
end
