class Hosting < ApplicationRecord
  has_attached_file :image, styles: {
  medium: "300x300>",
  thumb: "100x100>"
}
validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
validates :title,:image,:space,:register,:support,:price,presence: true
end
