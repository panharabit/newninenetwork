ActiveAdmin.register Slider do

    permit_params :saysomething,:title,:description,:image
  
    show do |t|
      attributes_table do
        row :saysomething
        row :title
        row :description
        row :image do
          slider.image? ? image_tag(slider.image.url, height: '100') : content_tag(:span, "No photo yet")
        end
      end
    end

    filter :title

    form :html=> {:enctype=>"multipart/form-data"} do |f|
      f.inputs do
        f.input :saysomething
        f.input :title
        f.input :description
        f.file_field :image, hint: f.slider.image? ? image_tag(slider.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
        actions
      end

    end

    index do
      selectable_column
      column link_to "ID" do |slider|
        link_to slider.id,admin_slider_path(slider)
      end
      column :saysomething
      column :title
      column :description
      column link_to "Image File" do |slider|
        # image_tag(post.image_file_name)
        image_tag(slider.image.url, height:'100')
      end
      column :created_at
      column :updated_at
      actions
    end
end
