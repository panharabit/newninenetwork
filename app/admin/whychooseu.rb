ActiveAdmin.register Whychooseu do
      menu parent: "Manage Content"
    permit_params :title,:description,:image

    show do |t|
      attributes_table do
        row :title
        row :description
        row :image do
          whychooseu.image? ? image_tag(whychooseu.image.url, height: '100') : content_tag(:span, "No photo yet")
        end
      end
    end

    filter :title

    form :html=> {:enctype=>"multipart/form-data"} do |f|
      f.inputs do
        f.input :title
        f.input :description
        f.file_field :image, hint: f.whychooseu.image? ? image_tag(whychooseu.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
        actions
      end

    end

    index do
      selectable_column
      column link_to "ID" do |why|
        link_to why.id,admin_whychooseu_path(why)
      end
      column :title
      column :description
      column link_to "Image File" do |why|
        # image_tag(post.image_file_name)
        image_tag(why.image.url, height:'100')
      end
      column :created_at
      column :updated_at
      actions
    end

end
