ActiveAdmin.register Contact do
      menu parent: "Manage Content"
    permit_params :email,:website,:phone,:address

    show do |t|
      attributes_table do
        row :email
        row :website
        row :phone
        row :address
      end
    end

    filter :title

    form :html=> {:enctype=>"multipart/form-data"} do |f|
      f.inputs do
      f.input :email
      f.input :website
      f.input :phone
      f.input :address
        actions
      end

    end

    index do
      selectable_column
      column link_to "ID" do |contact|
        link_to contact.id,admin_contact_path(contact)
      end
      column :email
      column :website
      column :phone
      column :address
      column :created_at
      column :updated_at
      actions
    end
end
