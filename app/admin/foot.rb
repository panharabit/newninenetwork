ActiveAdmin.register Foot do

  menu parent: "Manage Content"
    permit_params :image,:description,:copyright

    show do |t|
      attributes_table do
        row :copyright
        row :description
        row :image do
          foot.image? ? image_tag(foot.image.url, height: '100') : content_tag(:span, "No photo yet")
        end
      end
    end

    filter :title

    form :html=> {:enctype=>"multipart/form-data"} do |f|
      f.inputs do
        f.input :copyright
        f.input :description
        f.file_field :image, hint: f.foot.image? ? image_tag(foot.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
        actions
      end

    end

    index do
      selectable_column
      column link_to "ID" do |foot|
        link_to foot.id,admin_foot_path(foot)
      end
      column :copyright
      column :description
      column link_to "Image File" do |foot|
        # image_tag(post.image_file_name)
        image_tag(foot.image.url, height:'100')
      end
      column :created_at
      column :updated_at
      actions
    end
end
