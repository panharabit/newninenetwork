ActiveAdmin.register Categoryblog do
  menu parent: "Category"
  permit_params :categoryname

  show do |t|
    attributes_table do
      row :categoryname

    end
  end

  filter :categoryname

  form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.input :categoryname
      actions
    end

  end

  index do
    selectable_column
    column link_to "ID" do |cate|
      link_to cate.id,admin_categoryblog_path(cate)
    end
    column :categoryname
    column :created_at
    column :updated_at
    actions
  end

end
