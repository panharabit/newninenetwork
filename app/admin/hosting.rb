ActiveAdmin.register Hosting do
  menu parent: "Manage Content"
    permit_params :title,:image,:space,:register,:support,:price

    show do |t|
      attributes_table do
        row :title
        row :space
        row :register
        row :support
        row :price
        row :image do
          hosting.image? ? image_tag(hosting.image.url, height: '100') : content_tag(:span, "No photo yet")
        end
      end
    end

    filter :title

    form :html=> {:enctype=>"multipart/form-data"} do |f|
      f.inputs do
        f.input :title
        f.input :space
        f.input :register
        f.input :support
        f.input :price
        f.file_field :image, hint: f.hosting.image? ? image_tag(hosting.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
        actions
      end

    end

    index do
      selectable_column
      column link_to "ID" do |hosting|
        link_to hosting.id,admin_hosting_path(hosting)
      end
      column :title
      column :space
      column :register
      column :support
      column :price
      column link_to "Image File" do |hosting|
        # image_tag(post.image_file_name)
        image_tag(hosting.image.url, height:'100')
      end
      column :created_at
      column :updated_at
      actions
    end
end
