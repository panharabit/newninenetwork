ActiveAdmin.register Ourteam do
    menu parent: "Manage Content"
    permit_params :name,:position,:description,:image

    show do |t|
      attributes_table do
        row :name
        row :position
        row :description
        row :image do
          ourteam.image? ? image_tag(ourteam.image.url, height: '100') : content_tag(:span, "No photo yet")
        end
      end
    end

    filter :name

    form :html=> {:enctype=>"multipart/form-data"} do |f|
      f.inputs do
        f.input :name
        f.input :position
        f.input :description
        f.file_field :image, hint: f.ourteam.image? ? image_tag(ourteam.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
        actions
      end

    end

    index do
      selectable_column
      column link_to "ID" do |ourteam|
        link_to ourteam.id,admin_ourteam_path(ourteam)
      end
      column :name
      column :position
      column :description
      column link_to "Image File" do |ourteam|
        # image_tag(post.image_file_name)
        image_tag(ourteam.image.url, height:'100')
      end
      column :created_at
      column :updated_at
      actions
    end

end
