ActiveAdmin.register Blog do
    menu parent: "Manage Content"
  permit_params :title,:description,:image,:categoryblog_id,:post_date
  show do |t|
    attributes_table do
      row :title
      row :description
      row :image do
        blog.image? ? image_tag(blog.image.url, height: '100') : content_tag(:span, "No photo yet")
      end
    end
  end

  filter :title

  form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.collection_select :categoryblog_id,Categoryblog.all,:id,:categoryname
      f.input :title
      f.input :description
      f.input :post_date, as: :datepicker
      f.file_field :image, hint: f.blog.image? ? image_tag(blog.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
      actions
    end

  end

  index do
    selectable_column
    column link_to "ID" do |blog|
      link_to blog.id,admin_blog_path(blog)
    end
    column :title
    column :description
    column link_to "Image File" do |blog|
      # image_tag(post.image_file_name)
      image_tag(blog.image.url, height:'100')
    end
    column :created_at
    column :updated_at
    actions
  end
end
