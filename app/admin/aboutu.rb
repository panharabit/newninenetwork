ActiveAdmin.register Aboutu do
  menu parent: "Manage Content"
  permit_params :title,:description,:image

  show do |t|
    attributes_table do
      row :title
      row :description
      row :image do
        aboutu.image? ? image_tag(aboutu.image.url, height: '100') : content_tag(:span, "No photo yet")
      end
    end
  end

  filter :title

  form :html=> {:enctype=>"multipart/form-data"} do |f|
    f.inputs do
      f.input :title
      f.input :description
      f.file_field :image, hint: f.aboutu.image? ? image_tag(aboutu.image.url, height:'100') : content_tag(:span,"Upload JPG/PNG/GIF image")
      actions
    end

  end

  index do
    selectable_column
    column link_to "ID" do |about|
      link_to about.id,admin_aboutus_path(about)
    end
    column :title
    column :description
    column link_to "Image File" do |about|
      # image_tag(post.image_file_name)
      image_tag(about.image.url, height:'100')
    end
    column :created_at
    column :updated_at
    actions
  end

end
